module.exports = function (grunt) {

    grunt.initConfig({
        exec: {
            updateWebDriver: {command: 'cd ./node_modules/grunt-protractor-runner/node_modules/.bin && webdriver-manager update '}
        },
        protractor: {
            options: {
                keepAlive: true,
                noColor: false
            },
            iis: {
                options: {
                    configFile: "./protractor.conf.js"
                }
            }
        }
    });

    // This command registers the default task which will install bower packages into dist/lib
    grunt.registerTask('run-tests', ['exec:updateWebDriver','protractor:iis']);

    // The following line loads the grunt plugins.
    // This line needs to be at the end of this this file.
    grunt.loadNpmTasks('grunt-protractor-runner');
    grunt.loadNpmTasks('grunt-exec');

};