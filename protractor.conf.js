exports.config = {
    specs: ['./tests/**/*.spec.js'],
    baseUrl: 'http://localhost:63342/ProtractorSuite/test_site/index.html#/',
    framework: 'jasmine2',
    allScriptsTimeout: 10000,
    capabilities: {
        'browserName': 'chrome'
    },
    onPrepare: function() {
        var SpecReporter = require('jasmine-spec-reporter');
        jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: 'all'}));
    },
    jasmineNodeOpts: {
        print: function() {}
    }
};