(function (angular) {
    'use strict';

    angular
        .module('testSite', [
            'ngRoute',
            'angular.filter'
        ]);

    angular
        .module('testSite')
        .config(['$routeProvider', routeConfig]);

    function routeConfig($routeProvider) {
        $routeProvider
            .when('/Animal/', {
                templateUrl:'animal.html'
            })
            .otherwise('/Animal/');
    }

    angular
            .module('testSite')
            .controller('AnimalController', AnimalController);

        AnimalController.$inject = [];

        /* jshint -W072 */
        function AnimalController() {
            /* jshint +W072 */
            var vm = this;

            vm.dogVisible= false;
            vm.message = 'I Can Haz Puppy';

            vm.showDog = function () {
                vm.dogVisible = true;
            };

            vm.dogs = [
                'Goofy the Poodle',
                'Sir Awesomness the Poodle',
                'Snuggles the Poodle',
                'Grouchy the Chihuahua',
                'Sleepy the Bloodhound'
            ];


        }


})(angular);
