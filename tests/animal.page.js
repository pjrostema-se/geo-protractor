var AnimalPage = function() {

    this.otherSpecs = element.all(by.repeater('otherSpec in vm.otherSpecs'));
    this.breadCrumbCaret= element(by.css('.breadcrumb span[class="caret"]'));
    this.allSpecs= element.all(by.repeater('spec in vm.specs | filter:searchName'));
    this.specBody= element(by.css('spec-body'));
    this.specBodyTabs = this.specBody.all(by.repeater('tab in details.tabs'));
    this.specPane = element(by.css('.col-md-9'));
    this.firstSpec = this.allSpecs.all(by.css('.search-item')).first();
    this.searchBox= element(by.model('searchName.name'));
    /*this.lowLink= element(by.linkText('Low'));*/
    this.firstSpec= this.allSpecs.first().element(by.tagName('a'));

};

module.exports = new AnimalPage();