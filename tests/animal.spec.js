describe('The Animal Page', function() {

    var animal = require('./animal.page.js');

    /**
     * This runs before each test and navigates the browser to the Animal view for us.
     * This helps keep our tests DRY and removes errors caused by copy/paste.
     */
    beforeEach(function () {
        browser.get('#Animal');
    });

    it('The dog element is hidden by default', function () {
        var dog = element(by.css('.dog'));
        expect(dog.isDisplayed()).toBe(false);
    });

    it('The dog element is hidden until user clicks the show dog button', function () {
        var dog = element(by.css('.dog'));
        expect(dog.isDisplayed()).toBe(false);
        var showDogButton = element(by.css('button.show-dog'));
        showDogButton.click();
        expect(dog.isDisplayed()).toBe(true);
    });

    it('The list of dogs is filtered by the input text box', function () {
        var input = element(by.model('dogName'));
        input.sendKeys('Poodle');
        var dogList = element.all(by.repeater('dog in vm.dogs | filter:dogName'));
        expect(dogList.count()).toBe(3);
    });

});
