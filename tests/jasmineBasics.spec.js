describe('Basic Jasmine Tests', function() {

    var cat = {
        hasTail:true
    };
    var animal = {
        hasTail:false
    };

    it('The world is sane', function () {
        expect(true).toBe(true);
    });

    it('The cat is not null', function () {
        expect(cat).not.toBe(null);
    });

    it("cat has a tail", function() {
        expect(cat.hasTail).toBe(true);
    });

    describe("Cat anatomy", function() {
        it("cat has a tail", function() {
            expect(cat.hasTail).toBe(true);
        });
    });

    describe("Animal anatomy", function() {
        it("animal does not have a tail", function() {
            expect(animal.hasTail).toBe(false);
        });
        describe("Cat anatomy", function() {
            it("cat has a tail", function() {
                expect(cat.hasTail).toBe(true);
            });
        });
    });
});